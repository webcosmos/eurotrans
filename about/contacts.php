<?php
/**
 * Created by PhpStorm.
 * User: 45-parallel.org
 * Date: 26.09.2018
 * Time: 11:18
 */
?>
<?require_once $_SERVER['DOCUMENT_ROOT']."/views/header.php";?>
    <body class="page">
        <main class="page__main content">
            <section class="tickets">
                <h2>Контакты:</h2>
                <p><h4>Адрес:</h4> г. Ставрополь, Старомарьевское шоссе 32ж</p>
                <p><h4>Телефон:</h4> 8-800-551-08-11</p>
                <p><h4>Время работы:</h4> ежедневно 8:00-20:00</p>
            </section>
        </main>
    </body>
<?require_once $_SERVER['DOCUMENT_ROOT']."/views/footer.php";?>