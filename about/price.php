<?php
/**
 * Created by PhpStorm.
 * User: 45-parallel.org
 * Date: 03.10.2018
 * Time: 15:28
 */
require_once $_SERVER['DOCUMENT_ROOT']."/views/header.php";?>
<?require_once $_SERVER['DOCUMENT_ROOT']."/views/header.php";?>
<body class="page">
<main class="page__main content">
    <section class="tickets">
        <div class="price-container">
            <h2>Маршруты</h2>
            <h3>Москва</h3>
            <div class="items_price">
                <span>Ставрополь - Москва 2700 руб</span>
                <span>Нефтекумск - Москва 2500 руб</span>
                <span>Левокумское - Москва 2500 руб</span>
                <span>Буденновск - Москва 2500 руб</span>
                <span>Благодарный - Москва 2500 руб</span>
                <span>Светлоград - Москва 2500 руб</span>
                <span>Курская - Москва 2500 руб</span>
                <span>Советская - Москва 2500 руб</span>
                <span>Новопавловск - Москва 2500 руб</span>
                <span>Георгиевск - Москва 2500 руб</span>
                <span>Пятигорск - Москва 2500 руб</span>
                <span>Мин-воды - Москва 2500 руб</span>
                <span>Александровское - Москва 2500 руб</span>
                <span>Рыздвяный - Москва 2500 руб</span>
                <span>Изобильный - Москва 2500 руб</span>
                <span>Новотроицкое - Москва 2500 руб</span>
                <span>Новотроицкое - Москва 2500 руб</span>
                <span>Солнечнодольск - Москва 2500 руб</span>
                <span>Новоалександровское - Москва 2500 руб</span>
            </div>
            <div class="items_price">
                <span>Москва - Ставрополь 2700 руб</span>
                <span>Москва - Нефтекумск 2500 руб</span>
                <span>Москва - Левокумское 2500 руб</span>
                <span>Москва - Благодарный 2500 руб</span>
                <span>Москва - Светлоград 2500 руб</span>
                <span>Москва - Курская 2500 руб</span>
                <span>Москва - Советская 2500 руб</span>
                <span>Москва - Новопавловск 2500 руб</span>
                <span>Москва - Георгиевск 2500 руб</span>
                <span>Москва - Пятигорск 2500 руб</span>
                <span>Москва - Мин-воды 2500 руб</span>
                <span>Москва - Александровское 2500 руб</span>
                <span>Москва - Рыздвяный 2500 руб</span>
                <span>Москва - Изобильный 2500 руб</span>
                <span>Москва - Новотроицкое 2500 руб</span>
                <span>Москва - Солнечнодольск 2500 руб</span>
                <span>Москва - Новоалександровск 2500 руб</span>
            </div>
            <h3>Грозный</h3>
            <div class="items_price">
                <span>Ставрополь - Грозный 1200 руб</span>
                <span>Невинномысск - Грозный</span>
                <span>Мин-воды - Грозный</span>
                <span>Пятигорск - Грозный</span>
                <span>Нальчик - Грозный</span>
                <span>Беслан - Грозный</span>
                <span>Назрань - Грозный</span>
            </div>

            <div class="items_price">
                <span>Грозный - Ставрополь 1200 руб</span>
                <span>Невинномысск - Ставрополь</span>
                <span>Мин-Воды - Ставрополь</span>
                <span>Пятигорск - Ставрополь</span>
                <span>Нальчик - Ставрополь</span>
                <span>Беслан - Ставрополь</span>
                <span>Назрань - Ставрополь</span>
            </div>
            <h3>Астрахань </h3>
            <div class="items_price">
                <span>Ставрополь - Астрахань 1453 руб</span>
                <span>Светлоград - Астрахань</span>
                <span>Ипатово - Астрахань</span>
                <span>Дивное - Астрахань</span>
                <span>Элиста - Астрахань</span>
                <span>Утта - Астрахань</span>
                <span>Хулхута - Астрахань</span>
                <span>Линейное - Астрахань</span>
                <span>невиномысск - астрахань 1453 руб</span>
            </div>
            <div class="items_price">
                <span>Астрахань - Линейное</span>
                <span>Астрахань - Хулкута</span>
                <span>Астрахань - Утта</span>
                <span>Астрахань - Элиста</span>
                <span>Астрахань - Дивное</span>
                <span>Астрахань - Ипатово</span>
                <span>Астрахань - Светлоград</span>
                <span>Астрахань - Ставрополь 1453 руб</span>
            </div>
        </div>
    </section>
</main>
</body>
<?require_once $_SERVER['DOCUMENT_ROOT']."/views/footer.php";?>
