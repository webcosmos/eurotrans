<?php
/**
 * Created by PhpStorm.
 * User: 45-parallel.rg
 * Date: 26.09.2018
 * Time: 11:55
 */
?>
<!DOCTYPE html>
<html>
<head>

		<!-- Google Tag Manager -->
		<script>(function(w,d,s,l,i){w[l]=w[l]||[];w[l].push({'gtm.start':
		new Date().getTime(),event:'gtm.js'});var f=d.getElementsByTagName(s)[0],
		j=d.createElement(s),dl=l!='dataLayer'?'&l='+l:'';j.async=true;j.src=
		'https://www.googletagmanager.com/gtm.js?id='+i+dl;f.parentNode.insertBefore(j,f);
		})(window,document,'script','dataLayer','GTM-WV29ZKK');</script>
		<!-- End Google Tag Manager -->

    <title>Евротранс - Билеты на автобус Ставрополь - Москва: купить, заказать, забронировать.</title>
    <meta name="description" content="Евротранс - Дешевые билеты на автобус от перевозчика. Ставрополь, Ставропольский край, Москва, Астрахань, Грозный. Забронируйте и оплатите билет на сайте.">
    <meta name="viewport" content="width=device-width,initial-scale=1.0">
    <meta charset="utf-8">

    <link rel="stylesheet" href="/libs/swiper/swiper.min.css">
    <link rel="stylesheet" href="/css/style.css">
    <link rel="icon" href="/img/favicon/favicon.ico">
    <link rel="apple-touch-icon" sizes="180x180" href="/img/favicon/favicon.jpg">
    <script src="https://cdn.jsdelivr.net/npm/vue/dist/vue.js"></script>

</head>
 
<body class="page page_inner">

	<!-- Google Tag Manager (noscript) -->
	<noscript><iframe src="https://www.googletagmanager.com/ns.html?id=GTM-WV29ZKK"
	height="0" width="0" style="display:none;visibility:hidden"></iframe></noscript>
	<!-- End Google Tag Manager (noscript) -->